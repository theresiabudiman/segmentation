import cv2
import numpy as np
import os


base = os.getcwd()
classes = next(os.walk('.'))[1]
classes.remove('.git')

img = cv2.imread(os.path.join(base, 'Copper', 'H_07.bmp'), 0)
print(len(np.unique(img)))
img = cv2.imread(os.path.join(base, 'Copper', 'T_14.bmp'), 0)
print(len(np.unique(img)))
img = cv2.imread(os.path.join(base, 'Copper', 'H_08.bmp'), 0)
print(len(np.unique(img)))
img = cv2.imread(os.path.join(base, 'Copper', 'T_01.bmp'), 0)
print(len(np.unique(img)))