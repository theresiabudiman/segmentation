import cv2
import numpy as np
import os
import csv
from tqdm import tqdm


base = os.getcwd()
classes = next(os.walk('.'))[1]
classes.remove('.git')
data_all = []
k = 2

#hough circle + kmeans
for i_class in range(len(classes)):
	seg_dir = 'segments_'+str(classes[i_class])
	os.mkdir(seg_dir)
	files = [f for f in os.listdir(os.path.join(base, classes[i_class])) if f[-3:] == 'bmp']

	for f in tqdm(files):
		img = cv2.imread(os.path.join(base, classes[i_class], f), 0)
		img_r = np.float32(img.reshape(img.shape[0]*img.shape[1]))

		criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.5)
		
		ret, label, center = cv2.kmeans(img_r,k,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)
		new_center = np.uint8([int(x!=min(center))*255 for x in center])
		res = new_center[label.flatten()]
		img_k = res.reshape((img.shape))

		kernel = np.ones((3,3),np.uint8)
		img_k = cv2.morphologyEx(img_k, cv2.MORPH_CLOSE, kernel, iterations = 10)

		img_k = cv2.medianBlur(img_k,5)
		cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
		try:	
			circles = cv2.HoughCircles(img_k,cv2.HOUGH_GRADIENT,1,minDist = 10000,param1=30,param2=10,minRadius=0,maxRadius=0)
			circles = np.uint16(np.around(circles))
			for i in circles[0,:]:
			    # draw the outer circle
			    cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
			    # draw the center of the circle
			    cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)
		except:
			print('oh no', f)

		f_name = f
		cv2.imwrite(os.path.join(seg_dir, f_name), cimg)




